# KspRemote

Control ksp aircraft using android device

## Requirements

  - android version > 9
  - bluetooth connection between android device and PC
  - rotation sensor on android

not working on some phones(lg vXX)

## Setup

### Connecting

you need to do this only first time

1. Remove all Profiles
2. Reboot the PC once (if you tried to connect once and it doesn't worked well)
3. Run KspRemote
4. Make sure there is a notification like..
```
KspRemote is Running
```
5. Go to android bluetooth settings and connect to PC

There will be a small bluetooth icon on phone's top right side
and Your phone will show up as an input device on your PC
if not try again from first stage with doing second stage

your phone may not support this

### Mapping

before mapping analog axis to ksp You need to install [AFBW](https://forum.kerbalspaceprogram.com/index.php?/topic/175359-18x-afbw-revived-joystick-controller-mod/) as KSP has bug with analog inputs
__DONT USE KSPs INPUT SETTING__

#### Action Groups

##### Engines
set action group like...


| Switch                | #1     | #2     | #3     | #4     |
|-----------------------|--------|--------|--------|--------|
| Action Group(on, off) | (1, 2) | (3, 4) | (5, 6) | (7, 8) |


![alt text](https://i.ibb.co/16M4LZh/357.png)
![alt text](https://i.ibb.co/gjj1bMb/358.png)

##### Flaps
better use with FAR


| Up | Down |
|----|------|
| 9  | 10   |


![alt text](https://i.ibb.co/BsRpkbv/359.png)
![alt text](https://i.ibb.co/yNzVyGW/360.png)

##### Controls
1. Select ``Ksp Remote`` and click ``Presets``

![alt text](https://i.ibb.co/5vYMGsy/361.png)
2. You need to add axis like this

![alt text](https://i.ibb.co/9cBLLFG/363.png)
![alt text](https://i.ibb.co/jJ4jTJ4/362.png)

On this screen you need to press buttons several times that matches to axis in KspRemote's settings till it assigns

|    | axis |
|----|------|
| x  | #1   |
| y  | #2   |
| z  | #3   |
| Rx | #4   |


![alt text](https://i.ibb.co/mCWJJmP/Screenshot-20200216-002938-Ksp-Remote.png)

3. Go to ``Mod Settings`` and make AFBW input overrides throttle
![alt text](https://i.ibb.co/7JWZ1Ft/365.png)


The MIT License (MIT)
=====================

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

