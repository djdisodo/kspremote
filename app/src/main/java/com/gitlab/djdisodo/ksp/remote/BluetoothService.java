package com.gitlab.djdisodo.ksp.remote;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHidDevice;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.Nullable;
import com.github.bluetoothhid.bluetooth.HidDataSender;
import com.github.bluetoothhid.input.GamePad;
import com.github.bluetoothhid.input.Keyboard;

public class BluetoothService extends Service {

	private NotificationChannel notificationChannel;

	public class Binder extends android.os.Binder {
		public void setCallback(BluetoothHidDevice.Callback callback) {
			BluetoothService.this.callback = callback;
		}
		public HidDataSender getHidDataSender() {
			return hidDataSender;
		}
	}
	private BluetoothHidDevice.Callback callback = null;
	private HidDataSender hidDataSender;
	@Nullable
	@Override
	public Binder onBind(Intent intent) {
		System.out.println("Bind");
		return new Binder();
	}

	@Override
	public void onCreate() {
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationChannel = new NotificationChannel("bluetooth_hid_service", getString(R.string.bluetooth_hid_service), NotificationManager.IMPORTANCE_LOW);
		notificationChannel.setDescription("des");
		notificationChannel.enableLights(false);
		notificationChannel.enableVibration(false);
		notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
		notificationManager.createNotificationChannel(notificationChannel);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Notification notification = new Notification.Builder(this, "bluetooth_hid_service").build();
		startForeground(1, notification);
		hidDataSender = new HidDataSender(getString(R.string.hid_name), getString(R.string.hid_description), getString(R.string.hid_providor), BluetoothHidDevice.SUBCLASS2_JOYSTICK, new BluetoothHidDevice.Callback() {
			@Override
			public void onAppStatusChanged(BluetoothDevice pluggedDevice, boolean registered) {
				if (callback != null) {
					callback.onAppStatusChanged(pluggedDevice, registered);
				}
			}

			@Override
			public void onConnectionStateChanged(BluetoothDevice device, int state) {
				if (callback != null) {
					callback.onConnectionStateChanged(device, state);
				}
			}
		});
		hidDataSender.register(GamePad.class);
		hidDataSender.register(Keyboard.class);
		BluetoothAdapter.getDefaultAdapter().getProfileProxy(getApplicationContext(), hidDataSender, BluetoothProfile.HID_DEVICE);
		return START_STICKY;
	}

	@Override
	public boolean onUnbind(Intent intent) {
		stopSelf();
		return true;
	}
}
