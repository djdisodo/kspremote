package com.gitlab.djdisodo.ksp.remote;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHidDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.Switch;

import com.github.bluetoothhid.bluetooth.HidDataSender;
import com.github.bluetoothhid.input.GamePad;
import com.github.bluetoothhid.input.Keyboard;
import com.github.bluetoothhid.keyboard.Key;
import com.github.bluetoothhid.keyboard.KeyboardHelper;
import com.github.bluetoothhid.keyboard.Modifier;
import com.gitlab.djdisodo.ksp.remote.listener.BluetoothDeviceSelectListener;
import com.gitlab.djdisodo.ksp.remote.listener.OnCheckedListener;
import com.gitlab.djdisodo.ksp.remote.listener.OnGenericMotionListener;
import com.gitlab.djdisodo.ksp.remote.listener.RotationListener;
import com.h6ah4i.android.widget.verticalseekbar.VerticalSeekBar;

import java.util.ArrayList;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

	private BluetoothAdapter bluetoothAdapter;

	private HidDataSender hidDataSender;

	private ArrayList<View> buttons;

	final private RotationListener rotationListener = new RotationListener(this);



	private ServiceConnection serviceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			hidDataSender = ((BluetoothService.Binder)service).getHidDataSender();
			((BluetoothService.Binder)service).setCallback(new BluetoothHidDevice.Callback() {
				@Override
				public void onConnectionStateChanged(BluetoothDevice device, int state) {
					Log.d("dd", String.valueOf(state));
					runOnUiThread(() -> {
						Button button = (Button) findViewById(R.id.connect);
						switch (state) {
							case 0:
							case 3:
								button.setText(R.string.text_connect);
								break;
							case 1:
								button.setText(R.string.text_connecting);
								break;
							case 2:
								button.setText(device.getName());
								break;
						}
					});
				}
			});
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		((Switch) findViewById(R.id.engine_switch_1)).setOnCheckedChangeListener(new OnCheckedListener(this, KeyboardHelper.getKey('1'), KeyboardHelper.getKey('2')));
		((Switch) findViewById(R.id.engine_switch_2)).setOnCheckedChangeListener(new OnCheckedListener(this, KeyboardHelper.getKey('3'), KeyboardHelper.getKey('4')));
		((Switch) findViewById(R.id.engine_switch_3)).setOnCheckedChangeListener(new OnCheckedListener(this, KeyboardHelper.getKey('5'), KeyboardHelper.getKey('6')));
		((Switch) findViewById(R.id.engine_switch_4)).setOnCheckedChangeListener(new OnCheckedListener(this, KeyboardHelper.getKey('7'), KeyboardHelper.getKey('9')));
		final SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		sensorManager.registerListener(rotationListener, sensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR), SensorManager.SENSOR_DELAY_GAME);
		((Switch) findViewById(R.id.gyro_control)).setOnCheckedChangeListener((buttonView, isChecked) -> {
			if (isChecked) {
				rotationListener.enable = true;
				rotationListener.recalibrate();
			} else {
				rotationListener.enable = false;
				rotationListener.recalibrate();
			}

		});

		findViewById(R.id.recalibrate_control).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				rotationListener.recalibrate();
			}
		});



		findViewById(R.id.connect).setOnClickListener((view) -> selectDevice());
		findViewById(R.id.preference).setOnClickListener((view) -> {
			startActivity(new Intent(this, SettingsActivity.class));
		});

		buttons = new ArrayList<View>();
		addButton(findViewById(R.id.rcs), KeyboardHelper.getKey('r'));
		addButton(findViewById(R.id.sas), KeyboardHelper.getKey('t'));
		addButton(findViewById(R.id.flaps_up), KeyboardHelper.getKey('0'));
		addButton(findViewById(R.id.flaps_down), KeyboardHelper.getKey('9'));

		groupAndListen(
				findViewById(R.id.rcs)
		);

		groupAndListen(
				findViewById(R.id.sas)
		);

		groupAndListen(
				findViewById(R.id.flaps_up),
				findViewById(R.id.flaps_down)
		);

		((VerticalSeekBar)findViewById(R.id.throttle_seekbar)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (!rotationListener.enable) {
					getHidDataSender().sendReport(new GamePad.Report((byte)0, com.github.bluetoothhid.gamepad.Button.NONE, (byte)0, (byte)0, getYaw(),  getThrottle()));
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {}
		});

		((SeekBar)findViewById(R.id.yaw)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (!rotationListener.enable) {
					getHidDataSender().sendReport(new GamePad.Report((byte)0, com.github.bluetoothhid.gamepad.Button.NONE, (byte)0, (byte)0, getYaw(),  getThrottle()));
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				seekBar.setProgress(0);
			}
		});

		try {
			getSupportActionBar().hide();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}


		this.bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (this.bluetoothAdapter == null) {
			return;
		}

		if (this.bluetoothAdapter.isEnabled()) {
			this.startService();
			this.selectDevice();
		} else {
			Intent i = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			this.startActivityForResult(i, 0);
		}

	}

	private void startService() {
		Intent intent = new Intent(getApplicationContext(), BluetoothService.class);

		startForegroundService(intent);
		bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
	}

	private void selectDevice() {
		Set<BluetoothDevice> bondedDevices = this.bluetoothAdapter.getBondedDevices();
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.select_bluetooth_device);
		Button button;
		LinearLayout linearLayout = new LinearLayout(this);
		linearLayout.setOrientation(LinearLayout.VERTICAL);
		linearLayout.setPadding(25, 25, 25, 25);
		linearLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		BluetoothDeviceSelectListener bluetoothDeviceSelectListener = new BluetoothDeviceSelectListener() {
			@Override
			public void onResult(BluetoothDevice device) {
				hidDataSender.connect(device);
			}
		};
		for (BluetoothDevice device : bondedDevices) {
			button = new Button(this, null, android.R.attr.buttonBarButtonStyle);
			button.setLayoutParams(layoutParams);
			button.setTag(device);
			button.setText(device.getName());
			button.setOnClickListener(bluetoothDeviceSelectListener);
			linearLayout.addView(button);
		}
		ScrollView scrollView = new ScrollView(this);
		scrollView.setLayoutParams(linearLayout.getLayoutParams());
		scrollView.addView(linearLayout);
		builder.setView(scrollView);
		bluetoothDeviceSelectListener.alertDialog = builder.show();
	}

		@Override
		protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
			super.onActivityResult(requestCode, resultCode, data);
			switch (requestCode) {
				case 0:
					this.startService();
					this.selectDevice();
					break;
			}
		}

		@Override
		protected void onDestroy() {
			super.onDestroy();
			unbindService(serviceConnection);
		}

		public HidDataSender getHidDataSender() {
			return hidDataSender;
		}

		public ArrayList<View> getButtons() {
			return buttons;
		}

		public void addButton(View button, @Modifier @Nullable Byte modifier, @Key @Nullable Byte key) {
			if (modifier != null) {
				button.setTag(R.id.MODIFIER, modifier);
			}
			if (key != null) {
				button.setTag(R.id.KEY, key);
			}
			buttons.add(button);
		}

		public void addButton(View button, @Key @Nullable Byte key) {
			addButton(button, null, key);
		}

		public void groupAndListen(View... buttons) {
			for (View button : buttons) {
				button.setOnTouchListener(new OnGenericMotionListener(this, buttons));
			}
		}

		public Keyboard.Report generateReport() {
			Keyboard.Report keyboardReport = new Keyboard.Report();
			for (View button : getButtons()) {
				if (button.isPressed()) {
					if (button.getTag(R.id.MODIFIER) != null) {
						keyboardReport.addModifier((byte)button.getTag(R.id.MODIFIER));
					}
					if (button.getTag(R.id.KEY) != null) {
						keyboardReport.addKey((byte)button.getTag(R.id.KEY));
					}
				}
			}
			return keyboardReport;
		}

		@Override
		protected void onPause() {
			super.onPause();
			((Switch) findViewById(R.id.gyro_control)).setChecked(false);
		}
		public byte getThrottle() {
			return (byte)((VerticalSeekBar)findViewById(R.id.throttle_seekbar)).getProgress();
		}
		public byte getYaw() {
			return (byte)((SeekBar)findViewById(R.id.yaw)).getProgress();
		}

	@Override
	protected void onResume() {
		super.onResume();
		if (getHidDataSender() != null) {
			getHidDataSender().sendReport(new GamePad.Report((byte)0, com.github.bluetoothhid.gamepad.Button.NONE, (byte)0, (byte)0, (byte)0, getThrottle()));
		}
	}
}



