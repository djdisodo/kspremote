package com.gitlab.djdisodo.ksp.remote;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.view.MenuItem;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

import com.github.bluetoothhid.bluetooth.HidDataSender;
import com.github.bluetoothhid.input.GamePad;

public class SettingsActivity extends AppCompatActivity {
	private HidDataSender hidDataSender;

	final private ServiceConnection serviceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			BluetoothService.Binder binder = (BluetoothService.Binder) service;
			hidDataSender = binder.getHidDataSender();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {

		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Intent intent = new Intent(getApplicationContext(), BluetoothService.class);
		bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		preferences.edit()
				.putInt("pitch_sensitivity", preferences.getInt("pitch_sensitivity", 100))
				.putInt("roll_sensitivity", preferences.getInt("roll_sensitivity", 100))
				.apply();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings_activity);
		getSupportFragmentManager()
				.beginTransaction()
				.replace(R.id.settings, new SettingsFragment(this))
				.commit();
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
	}

	public static class SettingsFragment extends PreferenceFragmentCompat {
		private SettingsActivity settingsActivity;

		public SettingsFragment(SettingsActivity settingsActivity) {
			this.settingsActivity = settingsActivity;
		}
		@Override
		public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
			setPreferencesFromResource(R.xml.root_preferences, rootKey);
		}

		@Override
		public boolean onPreferenceTreeClick(Preference preference) {
			switch (String.valueOf(preference.getTitle())) {
				case "x":
					settingsActivity.hidDataSender.sendReport(new GamePad.Report((byte)0, (byte)0, (byte)127, (byte)0, (byte)0, (byte)0));
					settingsActivity.hidDataSender.sendReport(new GamePad.Report((byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0));
					break;
				case "y":
					settingsActivity.hidDataSender.sendReport(new GamePad.Report((byte)0, (byte)0, (byte)0, (byte)127, (byte)0, (byte)0));
					settingsActivity.hidDataSender.sendReport(new GamePad.Report((byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0));
					break;
				case "z":
					settingsActivity.hidDataSender.sendReport(new GamePad.Report((byte)0, (byte)0, (byte)0, (byte)0, (byte)127, (byte)0));
					settingsActivity.hidDataSender.sendReport(new GamePad.Report((byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0));
					break;
				case "Rx":
					settingsActivity.hidDataSender.sendReport(new GamePad.Report((byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)127));
					settingsActivity.hidDataSender.sendReport(new GamePad.Report((byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0));
					break;
			}
			if (preference.getKey() == null) {
				return true;
			}
			switch (preference.getKey()) {
				case "reset_sensitivity":
					SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(settingsActivity);
					preferences.edit()
							.putInt("pitch_sensitivity", 100)
							.putInt("roll_sensitivity", 100)
							.putBoolean("use_roll_yaw_mix", false)
							.putInt("roll_yaw_mix", 0)
							.apply();
					System.out.println("ss");
					getPreferenceScreen().removeAll();
					addPreferencesFromResource(R.xml.root_preferences);
					break;
			}
			return true;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				super.onBackPressed();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unbindService(serviceConnection);
	}
}