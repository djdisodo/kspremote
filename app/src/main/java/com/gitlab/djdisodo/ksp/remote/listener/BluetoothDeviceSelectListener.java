package com.gitlab.djdisodo.ksp.remote.listener;

import android.bluetooth.BluetoothDevice;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AlertDialog;

abstract public class BluetoothDeviceSelectListener implements View.OnClickListener {
	public AlertDialog alertDialog;
	@Override
	public void onClick(View v) {
		Button button = (Button) v;
		alertDialog.cancel();
		this.onResult((BluetoothDevice)button.getTag());
	}

	abstract public void onResult(BluetoothDevice device);
}
