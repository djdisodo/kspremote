package com.gitlab.djdisodo.ksp.remote.listener;

import android.widget.CompoundButton;

import com.github.bluetoothhid.input.Keyboard;
import com.github.bluetoothhid.keyboard.Key;
import com.gitlab.djdisodo.ksp.remote.MainActivity;

public class OnCheckedListener implements CompoundButton.OnCheckedChangeListener {

	private MainActivity mainActivity;
	private @Key byte on;
	private @Key byte off;

	public OnCheckedListener(MainActivity mainActivity, @Key byte on, @Key byte off) {
		this.mainActivity = mainActivity;
		this.on = on;
		this.off = off;
	}
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		Keyboard.Report keyboardReport = mainActivity.generateReport();
		if (isChecked) {
			keyboardReport.addKey(on);
		} else {
			keyboardReport.addKey(off);
		}
		mainActivity.getHidDataSender().sendReport(keyboardReport);
		mainActivity.getHidDataSender().sendReport(mainActivity.generateReport());
	}
}
