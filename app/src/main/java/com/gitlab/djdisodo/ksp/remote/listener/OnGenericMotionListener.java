package com.gitlab.djdisodo.ksp.remote.listener;

import android.view.MotionEvent;
import android.view.View;
import com.gitlab.djdisodo.ksp.remote.MainActivity;

public class OnGenericMotionListener implements View.OnTouchListener {
	private MainActivity mainActivity;
	private View[] group;

	public OnGenericMotionListener(MainActivity mainActivity, View[] group) {
		this.mainActivity = mainActivity;
		this.group = group;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				if (!v.isEnabled()) {
					break;
				}
				v.setPressed(true);
				for (View button :
						this.group) {
					button.setEnabled(v == button);
				}
				v.performClick();
				break;
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_CANCEL:
				v.setPressed(false);
				for (View button :
						this.group) {
					button.setEnabled(true);
				}
				break;
		}
		this.mainActivity.getHidDataSender().sendReport(mainActivity.generateReport());
		return true;
	}
}