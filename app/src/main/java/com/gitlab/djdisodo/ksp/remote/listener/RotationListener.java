package com.gitlab.djdisodo.ksp.remote.listener;

import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.Surface;

import androidx.preference.PreferenceManager;

import com.github.bluetoothhid.gamepad.Button;
import com.github.bluetoothhid.input.GamePad;
import com.gitlab.djdisodo.ksp.remote.MainActivity;
import com.gitlab.djdisodo.ksp.remote.R;
import com.h6ah4i.android.widget.verticalseekbar.VerticalSeekBar;

import javax.vecmath.Vector3f;

public class RotationListener extends Vector3f implements SensorEventListener {


	private MainActivity mainActivity;
	private boolean recalibrate = true;
	public boolean enable = false;
	final private float[] matrix = new float[9];
	final private float[] orientation = new float[3];
	final private Vector3f analog = new Vector3f();
	final private Vector3f values = new Vector3f();
	public RotationListener(MainActivity mainActivity) {
		this.mainActivity = mainActivity;
	}
	@Override
	public synchronized void onSensorChanged(SensorEvent event) {
		SensorManager.getRotationMatrixFromVector(matrix, event.values);
		SensorManager.getOrientation(matrix, orientation);
		if (recalibrate) {
			recalibrate = false;
			this.set(orientation);
		}
		if (!enable) return;
		values.set(orientation);
		values.sub(this);
		values.scale(2);
		if (mainActivity.getWindowManager().getDefaultDisplay().getRotation() == Surface.ROTATION_90) {
			values.scale(-1);
		}
		analog.setX(values.getY());
		analog.setY(values.getZ());
		SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(mainActivity);
		analog.setX(analog.getX() * preference.getInt("roll_sensitivity", 100));
		analog.setY(analog.getY() * preference.getInt("pitch_sensitivity", 100));
		analog.clamp(-127, 127);
		analog.setY(analog.getY() - getPitchTrim());
		analog.clamp(-127, 127);
		mainActivity.getHidDataSender().sendReport(new GamePad.Report((byte)0, Button.NONE, (byte)Math.round(analog.getX()), (byte)Math.round(analog.getY()), mainActivity.getYaw(), mainActivity.getThrottle()));
	}

	public void recalibrate() {
		recalibrate = true;
		mainActivity.getHidDataSender().sendReport(new GamePad.Report((byte)0, Button.NONE, (byte)0, (byte)0, (byte)0,  mainActivity.getThrottle()));
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {

	}

	private byte getPitchTrim() {
		return (byte)((VerticalSeekBar)mainActivity.findViewById(R.id.pitch_trim_seekbar)).getProgress();
	}


}
